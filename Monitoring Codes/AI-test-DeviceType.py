import numpy as np
import matplotlib.pyplot as plt
import tensorflow as tf
import time
import geoip2.database

"""
    Simulation with Authorized and Non-authorized Time Period)
"""

def get_dataset():
    """
        Method used to generate the dataset
    """
    #Numbers of row per class (nombre de donnée qu'on va avoir besoin par class)
    #Generate rows (Créer les entrées)
    
    pc = np.array([0])
    smartphone = np.array([1])
    mac = np.array([2])
    iphone = np.array([3])
    

    features = np.vstack([pc, smartphone, iphone, mac])
    
    targets = np.concatenate([np.zeros(2), np.zeros(2)+1])

    targets = targets.reshape(-1, 1)

    return features, targets

if __name__ == '__main__':
    features, targets = get_dataset()
    
    #plot points
#    plt.scatter(features[:], targets[:], c=targets)
#    plt.show()
    #print("Features", features)
    #print("Targets", targets)

    time.sleep(2)

    #Placeholers - se sont les entrées de notre graphe (ici il y en a 2)
    tf_features = tf.placeholder(tf.float32, shape=[None, 1]) #[nbr d'individus, nbr de caracteristique]
    tf_targets = tf.placeholder(tf.float32, shape=[None, 1]) #X

    #First Layer
    w1= tf.Variable(tf.random_normal([1, 3])) #X, 1
    b1 = tf.Variable(tf.zeros([3]))
    #Operations
    z1 = tf.matmul(tf_features, w1) + b1
    a1 = tf.nn.sigmoid(z1)
    
    #Output Neuron
    w2= tf.Variable(tf.random_normal([3, 1])) #1, 1
    b2 = tf.Variable(tf.zeros([1]))
    #Operations
    z2 = tf.matmul(a1, w2) + b2
    py = tf.nn.sigmoid(z2)

    cost = tf.reduce_mean(tf.square(py - tf_targets))

    correct_prediction = tf.equal(tf.round(py), tf_targets)
    accuracy = tf.reduce_mean(tf.cast(correct_prediction, tf.float32))

    optimizer = tf.train.GradientDescentOptimizer(learning_rate=0.1) #0.1
    train = optimizer.minimize(cost)

    sess = tf.Session()
    sess.run(tf.global_variables_initializer())
    
    
    #while sess.run(tf.equal(accuracy, [[1]]), feed_dict={tf_features: features,tf_targets: targets}) != [True]:  
    while sess.run(cost, feed_dict={tf_features: features,tf_targets: targets}) > 0.01:
        sess.run(train, feed_dict={
            tf_features: features,
            tf_targets: targets
        })

        print("accuracy=", sess.run(accuracy, feed_dict={
            tf_features: features,
            tf_targets: targets
        }))
    
#        acc = sess.run(accuracy, feed_dict={tf_features: features, tf_targets: targets})
#        plt.plot(e, acc, '-ok')
#        plt.pause(.000001)
#    plt.show
    
    print("\n")
    print("accuracy=", sess.run(accuracy, feed_dict={
        tf_features: features,
        tf_targets: targets
        }))

    print("cost =", sess.run(cost, feed_dict={
            tf_features: features,
            tf_targets: targets
            }))

    #************************************************************
    choix=""
    while choix != "x":
        choix = input("Enter the OS of the equipment: ")
        if choix == "Windows":
            vlr = 0
            print("C'est un PC")
        if choix == "Android":
            vlr = 1
            print("C'est un smartphone")
        if choix == "MAC":
            vlr = 2
            print("C'est un MacBook")
        if choix == "IOS":
            vlr = 3
            print("C'est un iPhone")
    
        featuresTest = np.vstack([vlr])
    
        z1 = tf.matmul(tf_features, w1) + b1
        a1 = tf.nn.sigmoid(z1)
        
        z2 = tf.matmul(a1, w2) + b2
        py = tf.nn.sigmoid(z2)
    
        resultat = tf.round(py)
            
        print("prevision=", sess.run(py, feed_dict={tf_features: featuresTest}))
        print("resultat=", sess.run(resultat, feed_dict={tf_features: featuresTest}))
        print("equal=", sess.run(tf.equal(resultat, [[1]]), feed_dict={tf_features: featuresTest}))
    
        if sess.run(tf.equal(resultat, [[1]]), feed_dict={tf_features: featuresTest}) == [True]:
            print("YES : The equipment can connect to the network")
        else:
            print("NON : The equipmentcannot connect to the network")
   