import numpy as np
import matplotlib.pyplot as plt
import tensorflow as tf
import time

"""
    Simulation with Authorized and Non-authorized Time Period)
"""

def get_dataset():
    """
        Method used to generate the dataset
    """
    #Numbers of row per class (nombre de donnée qu'on va avoir besoin par class)
    #Generate rows (Créer les entrées)
    
    #Création de la matrice de [0, 0] à [23, 59]
    features = np.array([0])
    ind = 0
    for h in range(24): #24
        #print(h, ":", m)
        t = np.array([h])
        if ind == 1:
            features = np.vstack([features, t])
        ind = 1
    
    targets = np.concatenate([np.zeros(7), np.zeros(8)+1, np.zeros(9)])

    targets = targets.reshape(-1, 1)

    return features, targets

if __name__ == '__main__':
    features, targets = get_dataset()
    #plot points
#    plt.scatter(features[:], targets[:], c=targets)
#    plt.show()
    #print("Features", features)
    #print("Targets", targets)

    time.sleep(2)

    #Placeholers - se sont les entrées de notre graphe (ici il y en a 2)
    #[nbr d'individus, nbr de caracteristique]
    tf_features = tf.placeholder(tf.float32, shape=[None, 1]) 
    tf_targets = tf.placeholder(tf.float32, shape=[None, 1]) #X

    #First Layer
    w1= tf.Variable(tf.random_normal([1, 3])) #X, 1
    b1 = tf.Variable(tf.zeros([3]))
    #Operations
    z1 = tf.matmul(tf_features, w1) + b1
    a1 = tf.nn.sigmoid(z1)
    
    #Output Neuron
    w2= tf.Variable(tf.random_normal([3, 1])) #1, 1
    b2 = tf.Variable(tf.zeros([1]))
    #Operations
    z2 = tf.matmul(a1, w2) + b2
    py = tf.nn.sigmoid(z2)

    cost = tf.reduce_mean(tf.square(py - tf_targets))

    correct_prediction = tf.equal(tf.round(py), tf_targets)
    accuracy = tf.reduce_mean(tf.cast(correct_prediction, tf.float32))

    optimizer = tf.train.GradientDescentOptimizer(learning_rate=0.1) #0.1
    train = optimizer.minimize(cost)

    sess = tf.Session()
    sess.run(tf.global_variables_initializer())
    
    
    e=0
    r=0
    x = np.array([0])
    y = np.array([0])
    while sess.run(cost, feed_dict={tf_features: features, tf_targets: targets}) > 0.01:
        #Si ça prend trop de temps recréer un autre réseau de neurones   
#        if r == 50000:
#            break
        sess.run(train, feed_dict={
            tf_features: features,
            tf_targets: targets
        })

        print("accuracy=", sess.run(accuracy, feed_dict={
            tf_features: features,
            tf_targets: targets
        }))
        print("r=", r)
        
        r+=1
        acc = sess.run(accuracy, feed_dict={tf_features: features, tf_targets: targets})
        if e%1000 == 0:
            a = np.array([e])
            b=np.array([acc])
        
            x = np.vstack([x, a])
            y = np.vstack([y, b])
        
            e+=1000
        
#        plt.plot(e, acc, '-ok')
#        plt.pause(0.00001)
#        
#    plt.show
            
    plt.plot(x[:], y[:])
    plt.show()
    
    print("\n")
    print("epochs=", r)
    print("accuracy=", sess.run(accuracy, feed_dict={
        tf_features: features,
        tf_targets: targets
        }))

    print("cost =", sess.run(cost, feed_dict={
            tf_features: features,
            tf_targets: targets
            }))
    print("w1=", sess.run(w1, feed_dict={
            tf_features: features,
            tf_targets: targets
        }))
    print("b1=", sess.run(b1, feed_dict={
            tf_features: features,
            tf_targets: targets
        }))
    print("w2=", sess.run(w2, feed_dict={
            tf_features: features,
            tf_targets: targets
        }))
    print("b2=", sess.run(b2, feed_dict={
            tf_features: features,
            tf_targets: targets
        }))

    time.sleep(2)
    vlr=0
    while vlr != 1:
        choix = input("What do you want to do 'analyse' or 'modify' time : ")
        if choix == 'modify': 
        #modify authorized period
            heure_st = int(input('Enter the start hour : '))
            heure_end = int(input('Enter the end hour : '))
            
            if heure_end<heure_st:
                p1 = heure_end + 1
                p2 = heure_st - p1
                p3 = 24 - p2 - p1
                targets = np.concatenate([np.zeros(p1)+1, np.zeros(p2), np.zeros(p3)+1])
                targets = targets.reshape(-1, 1)
            
                while sess.run(cost, feed_dict={tf_features: features,tf_targets: targets}) > 0.01:
                    sess.run(train, feed_dict={tf_features: features,tf_targets: targets})
                    print("accuracy=", sess.run(accuracy, feed_dict={
                            tf_features: features,
                            tf_targets: targets
                            }))  
            else:
                targets = np.concatenate([np.zeros(heure_st), np.zeros(heure_end-heure_st + 1)+1, np.zeros(24-heure_end-1)])
                targets = targets.reshape(-1, 1)
            
                while sess.run(cost, feed_dict={tf_features: features,tf_targets: targets}) > 0.01:
                    sess.run(train, feed_dict={tf_features: features,tf_targets: targets})
                    print("accuracy=", sess.run(accuracy, feed_dict={
                            tf_features: features,
                            tf_targets: targets
                            }))  
    
        #analyse a time (to see if it is authorized or not)             
        if choix == 'analyse':            
            heure = int(input('Enter the arrival time of the PC (hour) :'))

            #Test connection on the network
        
            pcTest = np.array([heure])
     
            featuresTest = np.vstack([pcTest])
    
            z1 = tf.matmul(tf_features, w1) + b1
            a1 = tf.nn.sigmoid(z1)
        
            z2 = tf.matmul(a1, w2) + b2
            py = tf.nn.sigmoid(z2)
            
            resultat = tf.round(py)
    
            print("prevision=", sess.run(py, feed_dict={tf_features: featuresTest}))
            print("resultat=", sess.run(resultat, feed_dict={tf_features: featuresTest}))
            print("equal=", sess.run(tf.equal(resultat, [[1]]), feed_dict={tf_features: featuresTest}))
            
            if sess.run(tf.equal(resultat, [[1]]), feed_dict={tf_features: featuresTest}).any() == [True]:
                print("YES! you can connect to the network")
            else:
                print("NON! you can't connect to the network")
                time.sleep(2)
                result = input("Voulez-vous l'autoriser à se connecter à cette heure ? (oui/non): ")
                if result == 'oui' or result == 'o':
                    #récupérer les indices dbut et fin de la période autorisée et l'indice où l'on veut apporter le changement
                    targetsTest = [[1]]
                    b=0
                    cpt=0
                    x=0
                    y=0
                    z=0
                    for i in features:
                        print("b=", b)
                        print("i=", i)
                        print("cpt=", cpt)
                        cpt +=1
                        if (featuresTest == i).all():
                            z=cpt
                            o=features[z]
#                          print("z=", z)
                            print("o=", o)
                        if sess.run(tf.equal(resultat, [[1]]), feed_dict={tf_features: [i]}) == [True] and b==0:
                            b=1
                            x=cpt
                            n=features[x]
#                           print("x=", x)
                            print("n=", n)
                        if sess.run(tf.equal(resultat, [[0]]), feed_dict={tf_features: [i]}) == [True] and b==1:
                            b=2
                            y=cpt-2
                            m=features[y]
#                           print("y=", y)
                            print("m=", m)
                    #Faire la re apprentissage en élargissant la période autorisée (en ajoutant des valeurs de 1)
                    if z<x:
                        v1 = z-1
                        v2 = y-v1
                        v3 = 24-y
                        targets = np.concatenate([np.zeros(v1), np.zeros(v2)+1, np.zeros(v3)])
                        targets = targets.reshape(-1, 1)
                    else:
                        v1 = x-1
                        v2 = z-v1
                        v3 = 24-z
                        targets = np.concatenate([np.zeros(v1), np.zeros(v2)+1, np.zeros(v3)])
                        targets = targets.reshape(-1, 1)
                    
                    while sess.run(cost, feed_dict={tf_features: features, tf_targets: targets}) > 0.01:
                        sess.run(train, feed_dict={tf_features: features, tf_targets: targets})
                        print("accuracy=", sess.run(accuracy, feed_dict={
                                tf_features: features,
                                tf_targets: targets
                                }))
                if result == 'non' or result == 'n':
                    pass
        else:
            pass