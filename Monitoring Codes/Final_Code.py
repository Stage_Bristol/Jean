#!/usr/bin/python
#-*- coding: utf-8 -*-
import numpy as np
import matplotlib.pyplot as plt
import tensorflow as tf
import time
import geoip2.database
import datetime
from datetime import datetime
import pytz
from pytz import timezone

"""
    Simulation with Authorized and Non-authorized Time Period)
"""

#fAIRE DU BATCH AVEC SOFTMAX CA PEUT-ETRE COOL!!! - ou pas

def get_dataset():
    """
        Method used to generate the dataset
    """
    #Numbers of row per class (nombre de donnée qu'on va avoir besoin par class)
    #Generate rows (Créer les entrées)
    
    #Création de la matrice de [0, 0] à [23, 59]
    features = np.array([1, 1, 1, 0, 0, 0])
    ind = 0
    for adIP in range(1, 3): #2 equipements
        for country in range(1, 3): #AU & GB
            for devType in range(1, 3): #Windows & Linux
                for versType in  range(2): # badVersion & goodVersion
                    for activ in range(2): #badURL & goodURL 
                        for hour in range(24): #24 heures
                            #print(h, ":", m)
                            t = [adIP, country, devType, versType, activ, hour]
                            if ind == 1:
                                features = np.concatenate([features, t])
                            ind = 1
    features = features.reshape((768,6))
    print("shape_features=", np.shape(features))

    targets1 = np.concatenate([np.zeros(192), np.zeros(96), np.zeros(48), np.zeros(24), np.zeros(6), np.zeros(12)+1, np.zeros(6)])
    targets2 = np.concatenate([np.zeros(192), np.zeros(96), np.zeros(96)])
    
    targets = np.concatenate([targets1, targets2])
    
    targets = targets.reshape(-1, 1)

    return features, targets, targets1, targets2

def trainNN(features, targets, seuil):
    #seuil = vlr
    e=0
    r=0
    x = np.array([0])
    y = np.array([0])
    while sess.run(cost, feed_dict={tf_features: features,tf_targets: targets}) > seuil:
        sess.run(train, feed_dict={tf_features: features,tf_targets: targets})
        result = sess.run(accuracy, feed_dict={
                tf_features: features,
                tf_targets: targets
                })
        print("accuracy", result)
        print("cost=", sess.run(cost, feed_dict={
                tf_features: features,
                tf_targets: targets
                }))
        print("r=", r)
        r+=1
        acc = sess.run(accuracy, feed_dict={tf_features: features, tf_targets: targets})
        if e%1000 == 0:
            a = np.array([e])
            b=np.array([acc])
            x = np.vstack([x, a])
            y = np.vstack([y, b])
            e+=1000
            
    #Display of the Graph        
    plt.plot(x[:], y[:])
    plt.show()
    
    print("\n")
    print("epochs=", r)
    
    return result

if __name__ == '__main__':
    #Code de Sabina 
    Tab=[]
    Tab.append([])
    Tab.append([])
    Tab.append([])
    Tab.append([])
    with open('/root/Desktop/AddIP/reception.py') as file :
	for a in file:
		Tab[0].append(a.strip())
		a2=list(set(Tab[0]))
    del(a2[0])

    with open('/root/Desktop/Heure/receptionT.py') as file :
	    for o in file:
		    Tab[1].append(o.strip())

    del(Tab[1][0])

    with open('/root/Desktop/DeType/receptionDt.py') as file :
	    for h in file:
		    Tab[2].append(h.strip())
	
    del(Tab[2][0])

    with open('/root/Desktop/Version/receptionV.py') as file :
	    for v in file:
		    Tab[3].append(v.strip())
    del(Tab[3][0])

    ad1 = Tab[0][1]
    h1 = str(Tab[1][0])
    os1 = Tab[2][0]
    v1 = Tab[3][0]

    pcpremier = [ad1,h1.lstrip('0'),os1,v1]
    print("PC1=", pcpremier)

    ad2 = Tab[0][2]
    h2 = str(Tab[1][1])
    os2 = Tab[2][1]
    v2 = Tab[3][1]
    pcsecond = [ad2,h2.lstrip('0'), os2,v2]
    print("PC2=", pcsecond)
    
    #Getting of Paramaters from get_dataset function
    features, targets, targets1, targets2 = get_dataset()
    reader = geoip2.database.Reader('/root/Desktop/AddIP/GeoLite2-City_20180501/GeoLite2-City.mmdb')    
    
    #Placeholers - se sont les entrées de notre graphe (ici il y en a 2)
    tf_features = tf.placeholder(tf.float32, shape=[None, 6]) #[nbr d'individus, nbr de caracteristique]
    tf_targets = tf.placeholder(tf.float32, shape=[None, 1]) #X
    
    #First Layer
    w1= tf.Variable(tf.random_normal([6, 36])) #X, 1
    b1 = tf.Variable(tf.zeros([36]))
    #Operations
    z1 = tf.matmul(tf_features, w1) + b1
    a1 = tf.nn.sigmoid(z1)
    
    #Output Neuron
    w2= tf.Variable(tf.random_normal([36, 24])) #1, 1
    b2 = tf.Variable(tf.zeros([24]))
    #Operations
    z2 = tf.matmul(a1, w2) + b2
    a2 = tf.nn.sigmoid(z2)
    
    #Output Neuron
    w3= tf.Variable(tf.random_normal([24, 12])) #1, 1
    b3 = tf.Variable(tf.zeros([12]))
    #Operations
    z3 = tf.matmul(a2, w3) + b3
    a3 = tf.nn.sigmoid(z3)
    
    #Output Neuron
    w4= tf.Variable(tf.random_normal([12, 1])) #1, 1
    b4 = tf.Variable(tf.zeros([1]))
    #Operations
    z4 = tf.matmul(a3, w4) + b4
    py = tf.nn.sigmoid(z4)
        
        
    cost = tf.reduce_mean(tf.square(py - tf_targets))
    
    correct_prediction = tf.equal(tf.round(py), tf_targets)
    accuracy = tf.reduce_mean(tf.cast(correct_prediction, tf.float32))
    optimizer = tf.train.GradientDescentOptimizer(learning_rate=2) #0.1
    train = optimizer.minimize(cost)
    
    sess = tf.Session()
    sess.run(tf.global_variables_initializer())
    
    trainNN(features, targets, 0.005)
    
    
    print("\n")
    print("accuracy=", sess.run(accuracy, feed_dict={
            tf_features: features,
            tf_targets: targets
            }))

    print("cost =", sess.run(cost, feed_dict={
            tf_features: features,
            tf_targets: targets
            }))
    print("w1=", sess.run(w1, feed_dict={
            tf_features: features,
            tf_targets: targets
            }))
    print("b1=", sess.run(b1, feed_dict={
            tf_features: features,
            tf_targets: targets
            }))
    print("w2=", sess.run(w2, feed_dict={
            tf_features: features,
            tf_targets: targets
            }))
    print("b2=", sess.run(b2, feed_dict={
            tf_features: features,
            tf_targets: targets
            }))
    
    #good version to compare for the Version type Monitoring
    v1 = "3.13.0-32-generic" 
        
    #Test avec un PC avec des caractéristiques @IP, temps en heure et type d'équipement
    pcs = [["164.11.203.57", 13, "Linux", v1, "www.facebook.com"], ["1.1.1.1", 13, "Linux", v1, "www.google.com"]]
    for pc in pcs:
        #@IP
        ipAddress = pc[0]
        #Time
        hour = int(pc[1])
        #Location
        response = reader.city(ipAddress)
        country = response.country.iso_code   
        #Device Type
        typeDevice = pc[2]
        #Version Type
        typeVersion = pc[3]
        #Activity
        activity = pc[4]
        
        
        
        #Analyze Stage
        print("Analyze for the device (", pc[0], "):")        
        #Display of the information of the device
        print("IP Address:", ipAddress)
        print("Country:", country)
        print("Device Type:", typeDevice)
        print("OS Version:", typeVersion)
        print("Activity:", activity)
        print("Connection Time:", hour, "h")
        
        #We put an indice on each parameters to ease the learning of the AI and for its predictions
        if ipAddress == "164.11.203.57": 
            numIP = 1
            if country == "AU":
                numCountry = 1
                if typeDevice == "Windows":
                    numTypeDevice = 1
                    if typeVersion == "v0":
                        numTypeVersion = 0
                        if activity == "www.facebook.com":
                            numActivity = 0
                            numModif = 0
                        if activity == "www.google.com":
                            numActivity = 1
                            numModif = 1
                    if typeVersion == v1:
                        numTypeVersion = 1
                        if activity == "www.facebook.com":
                            numActivity = 0
                            numModif = 2
                        if activity == "www.google.com":
                            numActivity = 1
                            numModif = 3
                if typeDevice == "Linux":
                    numTypeDevice = 2
                    if typeVersion == "v0":
                        numTypeVersion = 0
                        if activity == "www.facebook.com":
                            numActivity = 0
                            numModif = 4
                        if activity == "www.google.com":
                            numActivity = 1
                            numModif = 5
                    if typeVersion == v1:
                        numTypeVersion = 1
                        if activity == "www.facebook.com":
                            numActivity = 0
                            numModif = 6
                        if activity == "www.google.com":
                            numActivity = 1
                            numModif = 7
            if country == "GB":
                numCountry = 2
                if typeDevice == "Windows":
                    numTypeDevice = 1
                    if typeVersion == "v0":
                        numTypeVersion = 0
                        if activity == "www.facebook.com":
                            numActivity = 0
                            numModif = 8
                        if activity == "www.google.com":
                            numActivity = 1
                            numModif = 9
                    if typeVersion == v1:
                        numTypeVersion = 1
                        if activity == "www.facebook.com":
                            numActivity = 0
                            numModif = 10
                        if activity == "www.google.com":
                            numActivity = 1
                            numModif = 11
                if typeDevice == "Linux":
                    numTypeDevice = 2
                    if typeVersion == "v0":
                        numTypeVersion = 0
                        if activity == "www.facebook.com":
                            numActivity = 0
                            numModif = 12
                        if activity == "www.google.com":
                            numActivity = 1
                            numModif = 13
                    if typeVersion == v1:
                        numTypeVersion = 1
                        if activity == "www.facebook.com":
                            numActivity = 0
                            numModif = 14
                        if activity == "www.google.com":
                            numActivity = 1
                            numModif = 15
        if ipAddress == "1.1.1.1": #avoir en fct de l'ip du device
            numIP = 2
            if country == "AU":
                numCountry = 1
                if typeDevice == "Windows":
                    numTypeDevice = 1
                    if typeVersion == "v0":
                        numTypeVersion = 0
                        if activity == "www.facebook.com":
                            numActivity = 0
                            numModif = 16
                        if activity == "www.google.com":
                            numActivity = 1
                            numModif = 17
                    if typeVersion == v1:
                        numTypeVersion = 1
                        if activity == "www.facebook.com":
                            numActivity = 0
                            numModif = 18
                        if activity == "www.google.com":
                            numActivity = 1
                            numModif = 19
                if typeDevice == "Linux":
                    numTypeDevice = 2
                    if typeVersion == "v0":
                        numTypeVersion = 0
                        if activity == "www.facebook.com":
                            numActivity = 0
                            numModif = 20
                        if activity == "www.google.com":
                            numActivity = 1
                            numModif = 21
                    if typeVersion == v1:
                        numTypeVersion = 1
                        if activity == "www.facebook.com":
                            numActivity = 0
                            numModif = 22
                        if activity == "www.google.com":
                            numActivity = 1
                            numModif = 23
            if country == "GB":
                numCountry = 2
                if typeDevice == "Windows":
                    numTypeDevice = 1
                    if typeVersion == "v0":
                        numTypeVersion = 0
                        if activity == "www.facebook.com":
                            numActivity = 0
                            numModif = 24
                        if activity == "www.google.com":
                            numActivity = 1
                            numModif = 25
                    if typeVersion == v1:
                        numTypeVersion = 1
                        if activity == "www.facebook.com":
                            numActivity = 0
                            numModif = 26
                        if activity == "www.google.com":
                            numActivity = 1
                            numModif = 27
                if typeDevice == "Linux":
                    numTypeDevice = 2
                    if typeVersion == "v0":
                        numTypeVersion = 0
                        if activity == "www.facebook.com":
                            numActivity = 0
                            numModif = 28
                        if activity == "www.google.com":
                            numActivity = 1
                            numModif = 29
                    if typeVersion == v1:
                        numTypeVersion = 1
                        if activity == "www.facebook.com":
                            numActivity = 0
                            numModif = 30
                        if activity == "www.google.com":
                            numActivity = 1
                            numModif = 31
        print("-----------")
        print('numIP=', numIP)
        print('numCountry=', numCountry)
        print('numTypeoDevice=', numTypeDevice)
        print('numTypeVersion=', numTypeVersion)
        print('numActivity=', numActivity)
        print("-----------")
        #Test connection on the network
        pcTest = np.array([numIP, numCountry, numTypeDevice, numTypeVersion, numActivity, hour])
        featuresTest = np.vstack([pcTest])
            
        z1 = tf.matmul(tf_features, w1) + b1
        a1 = tf.nn.sigmoid(z1)
        z2 = tf.matmul(a1, w2) + b2
        a2 = tf.nn.sigmoid(z2)
        z3 = tf.matmul(a2, w3) + b3
        a3 = tf.nn.sigmoid(z3)
        z4 = tf.matmul(a3, w4) + b4
        py = tf.nn.sigmoid(z4)
        resultat = tf.round(py)
        
        print("-----------")
        print('numIP=', numIP)
        print('numCountry=', numCountry)
        print('numTypeoDevice=', numTypeDevice)
        print('numTypeVersion=', numTypeVersion)
        print('numActivity=', numActivity)
        print("-----------")
        print("Monitoring Result:")
        print("prevision=", sess.run(py, feed_dict={tf_features: featuresTest}))
        resultat = sess.run(resultat, feed_dict={tf_features: featuresTest})
        print("resultat=", resultat)
        equal = sess.run(tf.equal(resultat, [[1]]), feed_dict={tf_features: featuresTest})
        print("equal=", equal)
        print("\n")
        time.sleep(5)
            
       #Modify Location
        vlrLocation = int(input("Do you want to authorize(1) or unauthorized(0) a Country, or do nothing(2)? :"))
        if vlrLocation == 2:
            pass
        else:
            nameCountry = input("Type the country on alpha-2 code (ex: AU):")
            if typeVersion == v1: #Good OS Version
                if numIP == 1: #@IP 1
                    if numCountry == 1: #AU
                        targets[192:216] = vlrLocation #MODIFIER PAR RAPPPORT à LA VERSION v1 (la bonne version)
                    if numCountry == 2: # GB
                        targets[384:408] = vlrLocation #MODIFIER PAR RAPPPORT à LA VERSION v1 (la bonne version)
                if numIP == 2: #@IP 2
                    if numCountry == 1: #AU
                        targets[576:608] = vlrLocation #MODIFIER PAR RAPPPORT à LA VERSION v1 (la bonne version)
                    if numCountry == 2: #GB
                        targets[768:792] = vlrLocation #MODIFIER PAR RAPPPORT à LA VERSION v1 (la bonne version)
                trainNN(features, targets, 0.005)
            else:
                print("OS not update")
        
        #Modify Actitvity Monitoring 
        vlrActivity = int(input("Do you want to authorize(1) or unauthorized(0) an Activity, or do nothing(2)? :"))
        if vlrActivity == 2:
            pass
        else:
            nameActivity = input("Type the URL (ex: www.facebook.com):")
            if typeVersion == v1:
                if numIP == 1: #@IP 1
                    if numCountry == 1: #AU
                        if numActivity == 1:  #FB
                            targets[168:192] = vlrActivity 
                        if numActivity == 2: #Google
                            targets[192:216] = vlrActivity
                    if numCountry == 2: #GB
                        if numActivity == 1: #FB
                            targets[360:384] = vlrActivity
                        if numActivity == 2: #Google
                            targets[384:408] = vlrActivity
                if numIP == 2: #@IP 2
                    if numCountry == 1: #AU
                        if numActivity == 1: #FB
                            targets[552:576] = vlrActivity
                        if numActivity == 2: #Google
                            targets[576:600] = vlrActivity
                    if numCountry == 2: #GB
                        if numActivity == 1: #FB
                            targets[744:768] = vlrActivity
                        if numActivity == 2: #Google
                            targets[768:792] = vlrActivity
                trainNN(features, targets, 0.002)
            else:
                print("OS not update")
        
        #Modify Time Monitoring
        rTime = input("Do you want to modify the authorize period? (y/n):")
        if rTime == "y":
            heure_st = int(input('Enter the start hour : '))
            heure_end = int(input('Enter the end hour : '))
            if typeVersion == v1:
                if heure_end<heure_st: 
                    p1 = heure_end + 1
                    p2 = heure_st - p1 +1
                    p3 = 24 - p2 - p1
                    k1 = numModif*24
                    k2 = numModif*24+p1
                    k3 = numModif*24+p2
                    k4 = numModif*24+p3
                    targets[k1:k2] = 1
                    targets[k2:k3] = 0
                    targets[k3:k4] = 1
                else:
                    k1 = numModif*24
                    k2 = numModif*24+heure_st
                    k3 = numModif*24+heure_end + 1
                    k4 = numModif*24+24
                    targets[k1:k2] = 0
                    targets[k2:k3] = 1
                    targets[k3:k4] = 0
                print("k1=", k1)
                print("k2=", k2)
                print("k3=", k3)
                print("k4=", k4)
                targets = targets.reshape(-1, 1)
                #Entraînement avec la nouvelle période autorisée
                
                trainNN(features, targets, 0.001)
            else:
                print("\n***OS no update***\n")
        else:
            pass
        
        #Re-test connection on the network wuth settings
        pcTest = np.array([numIP, numCountry, numTypeDevice, numTypeVersion, numActivity, hour])
        featuresTest = np.vstack([pcTest])
            
        z1 = tf.matmul(tf_features, w1) + b1
        a1 = tf.nn.sigmoid(z1)
        z2 = tf.matmul(a1, w2) + b2
        a2 = tf.nn.sigmoid(z2)
        z3 = tf.matmul(a2, w3) + b3
        a3 = tf.nn.sigmoid(z3)
        z4 = tf.matmul(a3, w4) + b4
        py = tf.nn.sigmoid(z4)
        resultat = tf.round(py)
        
        #Analyze Stage
        print("Analyze for the device (", pc[0], "):")
        response = reader.city(ipAddress)
        country = response.country.iso_code
        #Display of the information of the device
        print("IP Address", ipAddress)
        print("Country:", country)
        print("Device Type:", typeDevice)
        print("OS Version:", typeVersion)
        print("Activity:", activity)
        print("Connection Time:", hour, "h")
        print("-----------")
        print('numIP=', numIP)
        print('numCountry=', numCountry)
        print('numTypeoDevice=', numTypeDevice)
        print('numTypeVersion=', numTypeVersion)
        print('numActivity=', numActivity)
        print("-----------")
        print("Monitoring Result:")
        print("prevision=", sess.run(py, feed_dict={tf_features: featuresTest}))
        resultat = sess.run(resultat, feed_dict={tf_features: featuresTest})
        print("resultat=", resultat)
        equal = sess.run(tf.equal(resultat, [[1]]), feed_dict={tf_features: featuresTest})
        print("equal=", equal)
        print("\n")
        time.sleep(5)
    