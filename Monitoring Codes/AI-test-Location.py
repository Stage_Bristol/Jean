import numpy as np
import matplotlib.pyplot as plt
import tensorflow as tf
import time
import geoip2.database
import datetime
from datetime import datetime
import pytz
from pytz import timezone

"""
    Simulation with Authorized and Non-authorized Time Period)
"""

def get_dataset():
    """
        Method used to generate the dataset
    """
    #Numbers of row per class (nombre de donnée qu'on va avoir besoin par class)
    #Generate rows (Créer les entrées)
    
    unknown = np.array([0]) #UNKNOWN
    us = np.array([1]) #Countries Non-allow
    ch = np.array([2]) #Countries Non-allow
    fr = np.array([3]) #Countries Allow
    gb = np.array([4]) #Countries Allow
    
    nonAllowCountry = np.vstack([unknown, us, ch])
    allowCountry = np.vstack([fr, gb])
    
    features = np.vstack([nonAllowCountry, allowCountry])
    
    targets = np.concatenate([np.zeros(3), np.zeros(2)+1])

    targets = targets.reshape(-1, 1)

    return features, targets

if __name__ == '__main__':
    features, targets = get_dataset()
    
    #plot points
#    plt.scatter(features[:], targets[:], c=targets)
#    plt.show()
    #print("Features", features)
    #print("Targets", targets)

    time.sleep(2)

    #Placeholers - se sont les entrées de notre graphe (ici il y en a 2)
    tf_features = tf.placeholder(tf.float32, shape=[None, 1]) #[nbr d'individus, nbr de caracteristique]
    tf_targets = tf.placeholder(tf.float32, shape=[None, 1]) #X

    #First Layer
    w1= tf.Variable(tf.random_normal([1, 3])) #X, 1
    b1 = tf.Variable(tf.zeros([3]))
    #Operations
    z1 = tf.matmul(tf_features, w1) + b1
    a1 = tf.nn.sigmoid(z1)
    
    #Output Neuron
    w2= tf.Variable(tf.random_normal([3, 1])) #1, 1
    b2 = tf.Variable(tf.zeros([1]))
    #Operations
    z2 = tf.matmul(a1, w2) + b2
    py = tf.nn.sigmoid(z2)

    cost = tf.reduce_mean(tf.square(py - tf_targets))

    correct_prediction = tf.equal(tf.round(py), tf_targets)
    accuracy = tf.reduce_mean(tf.cast(correct_prediction, tf.float32))

    optimizer = tf.train.GradientDescentOptimizer(learning_rate=0.1) #0.1
    train = optimizer.minimize(cost)

    sess = tf.Session()
    sess.run(tf.global_variables_initializer())
    
    
    #while sess.run(tf.equal(accuracy, [[1]]), feed_dict={tf_features: features,tf_targets: targets}) != [True]:  
    while sess.run(cost, feed_dict={tf_features: features,tf_targets: targets}) > 0.1:
        sess.run(train, feed_dict={
            tf_features: features,
            tf_targets: targets
        })

        print("accuracy=", sess.run(accuracy, feed_dict={
            tf_features: features,
            tf_targets: targets
        }))
    
#        acc = sess.run(accuracy, feed_dict={tf_features: features, tf_targets: targets})
#        plt.plot(e, acc, '-ok')
#        plt.pause(.000001)
#    plt.show
    
    print("\n")
    print("accuracy=", sess.run(accuracy, feed_dict={
        tf_features: features,
        tf_targets: targets
        }))

    print("cost =", sess.run(cost, feed_dict={
            tf_features: features,
            tf_targets: targets
            }))

#************************************************************
    countriesNonAuth = np. array(['UNKNOWN', 'US', 'CH'])
    countriesNonAuth = countriesNonAuth.reshape(-1,1)
    
    countriesAuth = np.array(['FR', 'GB'])
    countriesAuth = countriesAuth.reshape(-1,1)
    
    countries = np.vstack([countriesNonAuth, countriesAuth])
    vlr=0
    #Pays en fonction de l'@ IP
    while vlr != 1:
        reader = geoip2.database.Reader('.\GeoLite2-City_20180501\GeoLite2-City.mmdb')
        choix = int(input("Looking for the location of an address IP (0) or Modify the authorization of a connection for a country (1) "))
        if choix == 0:
            ipAddress = input("Type your IP address (Sabrina's IP@ --> 164.11.203.57): ")
            print("ip @ : ", ipAddress)
            response = reader.city(ipAddress) #US : 98.155.130.10
            print("Country:", response.country.name, "-", response.country.iso_code)
            print("City:", response.city.name)
            
            iso = response.country.iso_code
            numIso = 0
            i = 1
            for country in countries:
                if iso == country:
                    numIso = i
                else:
                    numIso = 0
                i+=1
            
            featuresTest = np.vstack([numIso])
    
            z1 = tf.matmul(tf_features, w1) + b1
            a1 = tf.nn.sigmoid(z1)
        
            z2 = tf.matmul(a1, w2) + b2
            py = tf.nn.sigmoid(z2)
            
            resultat = tf.round(py)
            
            print("prevision=", sess.run(py, feed_dict={tf_features: featuresTest}))
            print("resultat=", sess.run(resultat, feed_dict={tf_features: featuresTest}))
            print("equal=", sess.run(tf.equal(resultat, [[1]]), feed_dict={tf_features: featuresTest}))       
            
            if sess.run(tf.equal(resultat, [[1]]), feed_dict={tf_features: featuresTest}) == [True]:
                #Pays en fonction de l'heure
                format_heurePays = '%H'
                heurePC =16    #valeur à modifier pour le projet final
                heurePays=0
                timeZone = response.location.time_zone
                if timeZone == None:
                    print("YES : The PC in ", response.country.name, " can connect to the network but we cannot know if it uses a VPN")
                else:
                    print("Time zone = ", timeZone)
                    # Current time in timeZone
                    now_utc = datetime.now(timezone(timeZone)) #Enter the time zone
                    heurePays = int(now_utc.strftime(format_heurePays))
                    heurePC=heurePays #A enlever pour le projet final
                    print("\nHeure Pays:", heurePays)
                    print("Heure PC:", heurePC)
                    if heurePC == heurePays:
                        print("YES : The PC in ", response.country.name, " can connect to the network")
                    else:
                        print("NO : The PC use a VPN so it cannot be connect to the network")
            else:
                print("NO : The PC in ", response.country.name, " cannot connect to the network")
            
            
        if choix == 1:
            choix2 = input("'add' or 'delete' a country: ")
            if choix2 == 'add':
                isoAdd = input('Type the ISO code of the country you want to authorized (ex: FR):')
                countriesAuth = np.vstack([countriesAuth, isoAdd]) #Add new iso into the countriesAuth matrix
                countries = np.vstack([countriesNonAuth, countriesAuth])
                
                features = np.vstack([features, np.size(features)])
                targets = np.vstack([targets, 1])
                while sess.run(cost, feed_dict={tf_features: features,tf_targets: targets}) > 0.1:
                    sess.run(train, feed_dict={
                            tf_features: features,
                            tf_targets: targets
                            })
                    print("accuracy=", sess.run(accuracy, feed_dict={
                            tf_features: features,
                            tf_targets: targets
                            }))                         
            else:
                isoDelete = input('Type the ISO code of the country you want to non-authorized (ex: FR):')
                i = 0
                for country in countriesAuth:
                    i+=1
                    if country == isoDelete:
                        features = np.delete(features, (i-1), axis=0)
                        targets = np.delete(targets, (i-1), axis=0)
                        
                        countriesAuth = np.delete(countriesAuth, (i-1), axis=0)
                        
                        countriesNonAuth = np.vstack([countriesNonAuth, isoDelete])
                        countriesNonAuth = countriesNonAuth.reshape(-1,1)
                        
                        countries = np.vstack([countriesNonAuth, countriesAuth])
                        
                        taille1 = np.size(countriesNonAuth)
                        taille2 = np.size(countriesAuth)
                        np.concatenate([np.zeros(taille1), np.zeros(taille2)+1])

                         
                        while sess.run(cost, feed_dict={tf_features: features,tf_targets: targets}) > 0.1:
                            sess.run(train, feed_dict={
                                tf_features: features,
                                tf_targets: targets
                                })
                            print("accuracy=", sess.run(accuracy, feed_dict={
                                tf_features: features,
                                tf_targets: targets
                                })) 
                    