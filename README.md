# Internship at the University of the West of England at Bristol
*Jean SALAUN-PENQUER - Student in Networks and Telecoms at the IUT of Saint-Pierre*
<br/><br/><br/>

#### Tutor of the Internship
- Dr. Arabo
<br/>

#### Referent Teacher
- Dr. Jones <br/><br/> 

### Description of the Project
This project aims to show the nature of the subject with the work I will do during the internship.<br>
<br/>

### Subject of the Internship
Student will be involved in conducting research into the implications of AI in cybersecurity
solutions. This will involves answering questions such as how to vet the safety and/or security of AI applications without inspecting the
internal working of AI systems, even if such information is private? As some domain less or more tractable to monitor than others? The
research will lead to a proof of concept or road-map to possible solutions or more questions.
<br/><br/><br/>

### Sessions Goals
### Session 1 - 09/04/18 --> 13/04/18
Research information about the subject given.

### Session 2 - 16/04/18 --> 20/04/18
Create a planning of the week. Research with the information given by Dr. Arabo (IA monitoring on a SDN network).

### Session 3 - 23/04/18 --> 27/04/18
Looking for the information and creation of a Neuronal Network and if it is possible implement the Neuronal Network into the SDN Controller.

### Session 4 - 30/04/18 --> 04/05/18
Enhance the Neuronal Network and try to implement into SDN Controller in order to create an ecosystem with AI & SDN.

### Session 5 - 07/05/18 --> 11/05/18
Research on how to improve the code to allow full monitoring (host and service status).

### Session 6 - 14/05/18 --> 18/05/18
Search and conceive how the monitoring of authorized time periods of each network equipments.

### Session 7 - 21/05/18 --> 25/05/18
Improve code on authorized and non-authorized periods monitoring and thinking about Location monitoring.

### Session 8 - 28/05/18 --> 01/06/18
Prepare the schedule of Sessions 8-9-10 for Dr. Arabo, implement the Monitoring Time code in the Controller and see the Monitoring Location.

### Session 9 - 04/06/18 --> 08/06/18
Implement the code on monitoring on location in the Controller and start working on monitoring on the type of equipment.

### Session 10 - 11/06/18 --> 15/06/18
Implement the monitoring code on the equipment type in the Controller and start working on the equipment update monitoring.
<Finish the internship report and prepare the presentation>

### Session 11 - 18/06/18 --> 22/06/18
Implement code on (IP address montoring), time monitoring and if we have time monitoring on location.

### Session 12 - 25/06/18 --> 29/O6/18
Finish final code and testing with time, location, equipment type and OS update monitoring. 
Implement the final code into the controler and do the final tests to show Dr. Arabo.
Try to see if we have time to add monitoring on equipment activity.

### Session 13 & 14 - 02/07/18 --> 13/06/18
Writing of the SDN & IA project report.

### Content of the Git Project
- Planning of the week
- Weekly Reports
- Logbook
- Codes