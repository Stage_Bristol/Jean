# Logbook of the Internship
<br><br>
Lien du doc des recherches : https://docs.google.com/document/d/1akFWgBQwhauCWe2_U9qbeoGHXmQsUeUm6fdlMHEEQ7Y/edit#
<br><br>
## Session 1
### Day 1 (09/04/18)
Nous avons eu un entretien avec Mr Arabo. Il nous a donné les consignes pour le stage et il nous a fait la visite l'UWE.
On a commencé à faire le planning et demain nous devons faire les recherches sur nos sujets de stage.

### Day 2 (10/04/18)
Recherche sur ce que c'est la cybersécurité, l'IA et l'application de l'IA dans des solutions de cybersécurité. 
J'ai pu voir ce que c'était le "Machine learning". j'ai lu un livre qui s'appelle "An introdction to Artificial Intelligence" qui explique en gros
comment fonctionne une IA en générale. Il faudrait poursuivre les recherches et commencer à rédiger le planning prévisionnel du stage pour
le montrer à Dr. Arabo.

### Day 3 (11/04/18)
Recherche sur l'IA plus précisément sur la ML (Machine Learning). 
Les logiciels, les languages de programmation utilisés pour développer son ML (Python, C/C++, R, ...)
On a eu une Réunion de 15h à 16H30 "Research Centre Meeting" où il y a eu des présentation de projet dont le notre où Dr. Arabo a présenté.
On a posé des questions à Dr. Arabo concernant nos sujets. Il a dit que nous n'utiliserons pas de machine au sens matériel du terme mais
on fera des simulations. Il attend de nous qu'on touve nous même les logiciels/outils à utiliser et les informlations, et que en cas de problème
il pourrait nous aider si on a un souci.

### Day 4 (12/04/18)
Recherche appronfondie sur l'IA, "Machine Learning" et "Deep Learning". De plus j'ai pu voir dans le "Deep Learning" la différence entre
"Supervised Learning" and "Supervised Learning". Je vois un peu plus ce que ces termes signifie. 
Préparation de la reunion avec Dr. Arabo demain. Les slides sont pratiquement terminés et les notes aussi.
J'espère que la reunion de demain nous apprendra un peu plus sur les objectifs/outils qu'on devra faire/utiliser durant le stage.

### Day 5 (13/04/18)
J'ai fini les slides pour la réunion d'aujourd'hui avec Dr. Arabo et Sabrina. J'ai découvert d'autres informations concernant mes recherches 
sur l'IA, la ML et le DL. 
Ensuite on a eu la réunion cette après-midi vers 16h jusqu'à 17h. On a parlé de nos recherches et de ce qu'on a fait durant la semaine.
Puis Dr. Arabo nous a apporté des détails suplémentaire sur ce qu'on va faire durant le stage Sabrina et moi.
Sabrina devra s'occuper donc de créer une nouvelle architecture réseau appelé "SDN" et moi je devrai monitoré son réseau avec une IA
qui devra gérer automatiquement les évènements qui se produiront dans le réseau.

## Session 2
### Day 6 (16/04/18)
Recherche et lecture sur les documents que Dr. Arabo nous a envoyé. De plus on a établis un planning de la semaine a respecté.
On a rédigé le rapport de la semaine dernière (en anglais et en français) et uploader sur le git. Pour cette semaine je devrai
m'intéresser à de nouvelles notions : Immune Agent, l'IA dans un réseau SDN, ... (voir les issues).

### Day 7 (17/04/18)
Recherche et documentation sur les documents donnés par Dr. Arabo. On doit penser pour plus tard à des scénarios qui montreraient comment
notre réseau intélligent se comporterait. 

### Day 8 (18/04/18)
Documenation sur la Cybermix (Dr. Arabo Project) j'ai appris un peu plus comment marche le Deep Learning. On a modifié le planning pour
que soit mieux pour Sabrina et moi. J'était entrain de regarder une vidéo sur le Deep Learning (https://www.youtube.com/watch?v=og5m7f1seno).

### Day 9 (19/04/18)
J'ai regardé toutes les vidéos du tutoriel sur le DL. C'était super intéressant, j'ai pu voir comment fonctionnait concrétement un neuron.
Les variables qu'on utilise pour faire fonctionner le neuron, les algorithme utilisait pour l'apprentissage et la réduction de l'erreur.
J'ai pu apprendre pas mal de vocabulaire sur le DL. J'ai pu aussi faire exemple de DL en utilisant python. L'exemple c'est qu'on doit faire apprendre
à notre "neuron" (qui est en faite un programme) une liste de personnes malade et sain. J'ai pu afficher cela sur un graph, j'ai pu afficher
l'Accuracy (la précision) et le Cost (l'erreur total sur la dataset).

### Day 10 (20/04/18)
Préparation des deux diapos pour le meeting avec Dr. Arabo cette après-midi. Je parlerai d'un exemple du Deep Learning qui se nomme 
la régression logistique (en utilisant la fonction logistique - sigmoid comme paramètre d'activation). sa particularité c'est qu'en sortie
il possède que 2 valeurs (0 ou 1) binaires. La réunion s'est bien passé, Dr. Arabo a dit qu'on travailler trop et qu'il fallait qu'on prenne
du temps pour nous. Mdr. Objectif pour la prochaine session c'est de prendre en main un outil de DL (TensorFlow) pour qu'il fonctionne en tant
que filtre dans le réseau SDN et voir comme implémenter le code dans le Controller SDN (Les étapes 1 & 2 de la 2ème diapo de la présentation 20/04/2018.

## Session 3
### Day 11 (23/04/18)
Prise en main du logiciel TeXnicCenter, logiciel permettant d'écrire des rapports. J'ai fini d'écrire le rapport de la Session 2 et je suis
entrain de faire les rapport façon TeXnicCenter des 2 Sessions. Pour les prochaines fois faut que je vois comment installer TensorFlow, que
je continue de voir comment bien utiliser TeXnicCenter et continuer les recherches.

### Day 12 (24/04/18)
Installation du packet TensorFlow sur Anaconda et j'ai continué le tutoriel du français sur le Deep Learning. C'est très intéressant il explique
super bien! J'ai rencontré une erreur dans mon code lors du tutoriel je n'y arrive pas à trouver où elle se trouve (l'erreur c'est un problème
de "shape" d'une variable). Le tutoriel nous amène à contruire un Réseau de Neuron ce qui est bien pour la suite car je pense appliquer
cela à notre IA pour qu'elle puisse apprendre des choses très complexes.

### Day 13 (25/04/18)
J'ai continué le tuto fr sur le Réseau de Neuron. J'ai appris plus précisément commentle Réseau de Neuron fonctionne, comment triéer des images 
comportant des chiffres de 1 à 9. J'ai appris à utiliser les librairies tensorflow et les exemples de tensorflow (contenant la dataset avec 
les dessins de chiffres de 1 à 9). Je dois chercher maintenant quel type de donnée en Input je dois mettre pour que le Réseau de Neuron 
puisse l'utiliser et ainsi pouvoir prédir ce qu'il faut faire.

### Day 14 (26/04/18)
J'ai fini le tutoriel sur le Réseau de Neuron et aujourd'hui je me suis plutôt intéressé sur comment reproduire ce Réseau de Neuron dans mon
cas à moi c'est-à-dire en tant que moniteur, filtreur de paquet dans un réseau SDN et donc comment l'intégrer au sein du controlleur. J'ai réussi
à créer une sorte de simulation de dataset de base qui apprend les adresses IP autorisées et non-autorisées dans le réseau afin qu'il puisse
voir si tel appareil est un intrus ou pas. Objectif pour demain essayer de collaborer avec Sabrina afin de voir l'implémentation de l'IA dans 
le controlleur SDN.

## Session 4
### Day 15 (30/04/18)
Durant cette Session les objectifs sont de pouvoir améliorer et faire marcher le système de IA pour la Première simulation c'est-à-dire filtrage
de PC dans le réseau SDN avec en entrée les adresses hôtes de chaque PC. De plus il faudrait chercher et voir comment l'implémenter dans 
le système du Controller SDN. Aujourd'hui c'est plus de la recherche sur comment faire fonctionner le programme c'est compliqué car la dataset
(tableau d'apprentissage) est compliqué pour l'IA il faut essayer de trouver un moyen de le rendre plus facile pour l'IA pour qu'il puisse
bien apprendre sans faire le moin d'erreur possible (Précission >90%).

### Day 16 (01/05/18)
Rédaction du compte rendu de la session 3 en anglais et en français. J'ai réussi à améliorer le programme pour qu'il puisse foncitonner 
avec n'import quelle adresse hôte (de 0 à 255). Avec une précision de 100%. Maintenant il faudrait essayer de l'implémenter dans le Controller
SDN et essayer de faire une première simulation avec le Controller SDN et la Machine Learning.

### Day 17 (02/05/18)
Recherche et documentation sur comment améliorer l'IA (à partir de la 1er simulation). Trouver d'autres système de filtrage pour sécuriser le 
réseau. Voir plus précisément comment fonctionne les systèmes de sécurité aujourd'hui. J'ai vu des vidéos, conférences sur l'IA 
afin de voir si j'ai vu tout les angles de ce domaine là dans mon implication dans le projet.

### Day 18 (03/05/18)
Un peu comme hier recherche sur comment améliorer l'IA, on a regardé comment implémenter mon code (IA) dans celui de Sabrina (Controller et 
réseau SDN). On a essayé de le faire foncitonner mais il nous reste des problèmes au niveau des librairies mais une fois que cela sera 
résolu normalement la simulation pour se faire correctement. Préparation des diapos pour la réunion du 04/05/18 avec Dr. Arabo.

### Day 19 (04/05/18)
Préparation de la réunion avec Dr. Arabo, ce sera une presentation de session 3 & 4 on a décidé de les regrouper car elles sont très liées.
Je parlerai de la régression logistique appliqué dans une simulation (filtre les bonnes et mauvaises adresses IP). J'ai regardé une conférence
sur les réseaux à convolution, les problèmes qu'on rencontre et les solutions afin d'avoir une meilleur prédiction. C'est une vidéo qui dure
assez longtemps (2h-3h) mais elle reste très intéressente. Il faut que j'essaye de trouver d'autres manières à monitorer le réseau SDN. 
J'ai fait des recherches là-dessus et j'ai trouvé de bonne information (sur Wikipédia). Je pense que ça pourra aider dans la suite du projet.
Finalement Dr. Arabo ne pouvait pas venir à la réunion mais on le verra à a réunion de la prochaine session.

## Session 5
### Day 20 (07/05/18)
Recherche sur comment appliquer les différents systèmes de monitoring dans le réseau de manière théorique. Rédaction du rapport "Session 3 & 4",
sessions qu'on a décidé de les regrouper.

### Day 21 (08/05/18)
Recherche sur quelles seraient les entrées à mettre dans notre IA pour faire le monitoring. J'ai pensé à mettre l'adresse IP, le CPU,
le Storage et la RAM. Après avoir discuter avec Dr. Arabo, on a pu voir que l'adresse IP c'était bien qu'on l'utilise mais le reste à part
le CPU, ce n'est pas obligé de le monitoré. Dr. Arabo voudrait avoir un monitoring par rapport à quel PC à le droit d'entrer dans le réseau
et aussi autorisé ou non tel PC dans une salle (par rapport à la localisation des PCs). Avec Sabrina on va essayer de voir comment monitorer
la localisation des machines du réseau mais si celles-ci sont à l'étranger. Il faudrait aussi pouvoir différencier si telle machine est un PC,
Laptop ou bien un smartphone. J'ai réaussi à créer un réseau qui puisse apprendre les différents pourcentage par rapport à si un service 
est beaucoup utiliser (100%) ou non (0%). Là j'essaye de faire fonctionner avec les adresses IP le réseau utilisant la fonction d'activation
softmax qui d'après mes recherches serait beaucoup plus efficace que la fonction sigmoid.

### Day 22 (09/05/18)
Un peut comme hier je recherche toujours comment on fait pour monitorer la localisation d'une machine. De plus j'essaye de résoudre un problème avec Sabrina
qui est de faire foncitonner le programme qui utilise les librairies Tensorflow. Comme la VM de Sabrina qui simule le réseau SDN n'a pas 
beaucoup d'espace de stockage, il ne peut pas installer la librairie Tensorflow. J'essaye donc de voir si y a pas moyen de modifier la librairie
Tensorflow pour ne garder que les fonctions dont j'en réellement besoin. 

### Day 23 (10/05/18)
J'ai vu des conférences sur les réseaux à convolution de Devoxx qui sont très bien pour comprendre comment cela foncitonne. De plus j'ai continué
à améliorer mon programme pour qu'il puisse prendre d'autres éléments en paramètre pour permettre à l'IA de traiter plusieurs données en entrée.
C'est compliqué mais je commence à y arriver à faire foncitonner. De plus j'ai suivi les cours de udacity, ce sont les cours créés par Google
sur le Deep Learning.

### Day 24 (11/05/18)
J'ai fais un peu théorie sur les problèmes qu'on pouvoit rencontrer sur les réseaux à convolution lorsqu'on lui donne trop de données en entrées.
Puis j'ai préparer les diapositifs pour la réunion. J'ai l'impression d'être bloqué lors de mes recherches. Après la réunion avec Dr. Arabo,
Sabrina et moi avons compris ce que nous devions faire plus précisément. Mais pour ce que je dois faire pour la prochaine Session est encore
flou. Je pense que dès lundi je n'hésiterai pas à envoyer un mail à Dr. Arabo pour me donner des pistes sur ce que je dois plus concrètement.

## Session 6
### Day 25 (14/05/18)
J'ai commencé à faire le rapport de la Session 5. J'ai regardé par rapport à ce que Dr. Arabo nous a dit à la dernière réunion pour voir quelles
seraient les point à aborder durant la Session 6. Voulant être sûr j'irai demander demain à Dr. Arabo si c'est bon ou pas les points que je lui
montrerai ou si il faut les modifier ou en rajouter. J'ai vu aussi comment on faissait pour avoir le rapport en pdf avec TeXnicCenter et 
j'ai pu mettre les réferences sur les rapports de la Session 1 à 4.

### Day 26 (15/08/18)
J'ai commencé à voir comment le monitoring sur des périodes autorisées et non-autorisées pour fonctionner bien que je demanderai confirmation
à Dr. Arabo pour lui demander si c'est bien cela qui l'intéresse. J'ai pu voir Dr. Arabo, il m'a donné le disque dur pour Sabrina pour qu'elle
puisse réussir à faire fonctionner sa VM en augmentant l'espace de stokage de la VM. J'ai profité pour demander à Dr. Arabo si c'est bien le 
monitoring des périodes de temps. Il m'a dit oui et il m'a donné d'autres informations comme le monitoring sur la localisation de l'équipement,
sur sa nature (PC, laptop, smartphone,... + son type d'OS) et sur son activité au sein du réseau SDN.

### Day 25 (16/08/18)
J'ai rédigé le rapport de la session 5 puis j'ai mis de l'ordre sur le One Drive et un peu sur le Google Drive. J'ai revu un peu comment faire
pour le monitoring sur la période de temps. J'ai commencé à faire le rapport de stage en faisant la première page de converture et les parties
que je pourrais mettre dans le rapport.

### Day 26 (17/05/18)
J'ai commencé à faire la partie du code pour monitorer les périodes de temps (autorisée et non-autorisée). C'est compliqué il y a plein de
test qu'il faut faire afin d'avoir une accuracy proche de 1. De plus j'ai fait des test pour voir si en rajoutant des couches, des neurones,...
c'était mieux. J'arrive à peu près à me rapprocher d'une bonne accuracy au tour de 0.7 mais ce n'est pas assez. Faudrait réfléchir à comment 
améliorer le code et comment faire en sorte que l'admin lui apprenne alors que l'IA est en service (durant la phase de test - après son 
apprentissage avec la dataset).

### Day 27 (18/05/18)
J'ai pu améliorer le code en permettant à l'admin d'autoriser une période qui de base n'est pas autorisée à un PC. De plus j'ai pu voir 
que pour que l'IA apprenne bien, elle a besoin de traiter de nombreuses données sinon même si elle a plein de neurones et/ou plein de couches
cela ne servira à rien. On a eu notre reunion du quotidien. Dr. Arabo nous a expliqué la suite de ce qu'on devrait faire durant notre travail.
Pour commencer les données je dois les prendre comme si que c'était un String et pour plus tard les utiliser en s'aidant d'un tableau (voir 
cahier). De plus on a pu voir que certaines variables, l'admin peut les modifier comme l'ID de l'USER et les périodes de temps (authorisée).
La semaine pro je me concentrerai sur l'amélioration du monitoring sur les périodes de temps.

## Session 7
### Day 28 (21/05/18)
Rédaction de la Session précédente (Session 6). J'ai aussi fait des recherches pour savoir comment faire une graph qui affiche en temps réel
la progression de la précission en fonction du nombre d'epoch (1 epoch = 1 boucle d'apprentissage). J'ai aussi travaillé un peu sur mon rapport
de stage mai vite fait.

### Day 29 (22/05/18)
J'ai réussi à résoudre une des parties les plus problématique dans le code python pour le monitoring des temps de périodes en faisant en sorte 
que l'admin puisse autoriser l'accès ou non au réseau à une heure qui est par défaut non-authorisée. Pour cela lorsque l'admin autorisera,
il allongera le temps de l'equipement sur le réseau en fonction de l'heure qu'il a choisit (par exemple, de base mon smartphone à le droit 
d'être sur le réseau entre 7h et 17h mais comme je sais que j'aurai besoin d'avoir accès au réseau jusqu'à 20h, mon temps sur le réseau sera
modifier et j'aurai donc accès au réseau de 7h à 20h. Il me reste plus que faire la partie où l'admin décidera de réduire le temps de période
autorisée et ce sera bon pour cette partie.

### Day 30 (23/05/18)
J'ai continué à rédiger mon rapport j'ai commencé à faire l'introduction de mon rapport puis les remerciements. J'ai commencé à voir les paries que je pourrai
mettre dans la partie développement. J'ai revu ma page de couverture du rapport. Ensuite je suis passé au code avec le monitoring par temps de périodes. C'est 
bizarre car tout le code je le comprends mais ça me donne certaines erreures qu'il ne devrait pas y avoir. Du coup j'essaye de modifier le code pour voir
la source du problème et apparemment ce serait la partie où je définis mes variables m et n (les heures de début et de fin de la période autorisée). Mais 
il reste encore des problèmes, c'est embêtant.

### Day 30 (24/05/18)
J'ai trouvé le problème dans le code pour le monitoring de périodes de temps. J'ai pu tout débloqué maintenant le code est pratiquement bon tout le test on
pratiquement tous marchés. J'ai trouvé comment faire pour coder sur le monitoring de localisation, il y a une librairie qui se nomme geopy2 qui contient 
une database pouvant comparer n'importe quelle adresse IP en fonction du pays à laquelle elle est associé. J'ai fini les petits tests et j'ai commencé à faire 
le "vrai" code pour ce monitoring. D'ici la fin de cette session le monitoring de périodes sera terminé et je pourrai alors me concentrer sur le monitoring
de localisation.

### Day 31 (25/05/18)
J'ai fais du ménage dans le code Monitoring Period (J'ai enlever les lignes de code qui ne servaient à rien et j'ai fais en sorte d'éviter la répétition dans le
code). Au niveau du monitoring de localisation j'ai pu réussir à avoir une sorte de liste en modifiant juste le prémier octet (X.0.0.0 on modifie le X) et 
ça me donne pour chaque adresse sa localisation c'est pas très bien précis mais je pense le compléter en rajoutant l'heure actuel de l'equipement et le 
comparer à notre heure à nous afin de voir si il est vraiment bien dans ce pays là où dans un autre.

## Session 8 & 9
### Day 32 (28/05/18)
J'ai commencé à rédiger le rapport de la session 7, puis j'ai travaillé un peu sur le monitoring sur la Localisation voir comment je peux créer une dataset pour ce type
de monitoring. J'ai aussi écris toutes les parties de mon rapport de stage (celui de l'IUT) et j'ai commencé à rédiger certaines parties et corriger/modifier d'autres

### Day 33 (29/05/18)
Amélioration des codes sur le monitoring de temps et le monitoring de localisation. J'ai pu réussir à faire des graphes pour le rapport de la session 7 afin de comparer
les code pour le monitoring time celui avec en entrée l'heure et la minute et celui avec que l'heure en entrée. De plus j'ai temriné le rapport de la session 7, il me
manque plus de le traduire en anglais et de le faire sous texneccenter.

------------------Break------------------

### Day 34 (06/06/18)
Durant le Break nous étions parti visiter Londres. Nous avons donc pris en compte ces 5 jours dans notre emploie du temps afin qu'on puisse
ne pas prendre de retard sur notre travail. 
Aujourd'hui j'ai travailler sur le rapport de la Session 7 et je l'ai rédiger. J'ai continuer le rapport de stage (la partie présentation de 
l'entreprise). Et ensuite on a travailler sur le projet.

### Day 35 (07/06/18)
Aujourd'hui j'ai pu avancer sur mon rapport de stage j'ai pu bien entamer mon développement puis j'ai pu travailler un peu sur le projet 
sur le temps il faut régler quelques problèmes et sur la localisation pour repenser à la manière de monitorer sur ce paramètre. 

### Day 36 (08/06/18)
J'ai beaucoup avancer sur le rapport j'ai expliqué ce que j'ai fait plus précisément lors du monitoring du temps j'ai aussi vu un peu sur
la partie 3 et sur ce que je vais mettre en Annexes. J'ai encore quelques test à faire avec le monitoring time et je travaille en parallèle
sur le monitoring location.

## Session 10
### Day 37 (11/06/18)
Rédaction du rapport la partie Monitoring du temps dans le Développement.

### 12/06/18
Rédaction du rapport la partie Monitoring de la localisation dans le Développement. De plus j'ai rédigé le rapport de la Session 8 et 9 sur le Monitoring
sur la localisation.

### Day 38 (13/06/18)
J'ai bientôt terminé tout le rapport il me reste que la partie Conclusion qu'il faut que je travaille. J'ai commencé à traduire le rapport en Anglais.

### Day 39 (14/06/18)
Le rapport de stage a été terminé en français et en anglais. Je me suis intéressé sur la librairie platform de python et j'ai pu résusir à faire foncitonner
des fonctions permettant de récupérer le type d'OS et sa version. J'essaie de voir comment marche le batch.

### Day 40 (15/06/18)
Les rapports de stage en Français et en Anglais ont été finalisés après quelques modifications et vérifications.

## Session 11
### Day 41 (18/06/18 au 19/06/18)
Préparation de la soutenance de stage pour le mardi 19/06/18.

### Day 42 (20/06/18)
Je suis entrain d'améliorer le monitoring de temps pour qu'il puisse gérer le temps de 4 équipements.

### Day 43 (21/06/18)
Toujours entrain de travailler sur le code regroupant plusieurs équipements pour le Monitoring du temps. De plus on essaye de faire fonctionner le code sur la 
Localisation dans le Controller SDN afin de voir si cela fonctionne correctement.

### Day 44 (22/06/18)
Création du code pour le projet final. J'ai continuer à travailler pour assembler les monitoring ensemble et permettre le monitoring sur plusieurs équipement.

## Session 12
### Day 45 (25/06/18)
J'ai réussi à faire foncitonner le code final avec le monitoring sur le temps, la localisation et le type d'équipement. Il manque plus qu'à intégrer le code sur le
monitoring sur la mise à jour des équipement et si on a le temps l'activité des pcs (exemple si il se connecte sur FB ou sur Youtube).

### Day 46 (26/06/18)
J'ai fait des tests sur le code final avec le temps, la localisation et le type d'équipement afin de voir si le code final fonctionee corectement. Il y avait des
ajustement à faire, ça prend beaucoup de temps pour l'apprentissage mais il faut être patient. J'ai pu trouver les limites d'apprentissage (avec le coût) afin 
d'avoir un apprentissage rapide et efficace. Demain je m'intéresserai sur l'implémentation du monitoring sur le mise à jour de l'OS.

### Day 47 (27/06/18)
Nous avons pu finir le code final en ajoutant le monitoring sur la mise à jour des OS. De plus on a pu faire fonctionner le code final en 
l'implémentant dans le Controleur. 

### Day 48 (28/06/18)
J'ai travaillé sur le monitoring sur l'activité, pour cela je me suis intéressé à 2 types d'activités: les personnes se connectant à google (ce
qui est bon) et les personnes se connectant à facebook (ce qui n'est pas bon). J'ai réussi à le faire et il manque plus de confirmer
le code avec des tests.

### Day 49 (29/06/18)
On a terminé le code final avec le Monitoring sur le Temps, la Localisation, le type d'équipement, la mise à jour de l'OS et l'activité. Le
problème c'est que ça met beaucoup de temps à apprendre et je pense qu'il serait bien de trouver un bon réseau de neurones qui puisse
apprendre le plus rapidement possible (avec des bon configuration au niveau du biais et des poids).

## Session 12
### Day 50 (02/07/18)
Commencer à rédiger le rapport pour Dr. Arabo. Faire des recherches et rédiger le Background du rapport sur la partie de l'IA. J'ai pu voir
aussi la différence entre Introduction et Background.

### Day 51 (03/07/18)
On continue a travailler sur le Background, je continue aussi à faire des recherches pour ma bibliographie. On a établit un timetable pour
voir les objectifs qu'on doit faire chaque jour. 